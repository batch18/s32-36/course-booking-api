const express = require('express');
const router = express.Router();
let auth = require('./../auth')


const courseController = require ('./../controllers/courseController')


router.get('/active', (req, res) => {

	courseController.getAllActive().then(result => res.send(result));
})

router.get('/all', (req, res) => {

	courseController.getAll().then(result => res.send(result));
})



router.post('/addCourse', auth.verify, (req, res) => {
	courseController.addCourse(req.body).then(result => res.send(result));
})


// make a route to get single course
router.get('/:courseId', auth.verify, (req, res) => {
	courseController.findCourse(req.params.courseId).then(result => res.send(result));
})

// make route to update the course
router.put('/edit/:courseId', auth.verify, (req, res) => {
	courseController.editCourse(req.body, req.params.courseId).then(result => res.send(result));
})

// make a route to archive the course
router.put('/archiveCourse/:courseId', auth.verify, (req, res) => {
	courseController.archiveCourse(req.params.courseId).then(result => res.send(result));
})

// make a route to un-archive the course
router.put('/unarchiveCourse/:courseId', auth.verify, (req, res) => {
	courseController.unarchiveCourse(req.params.courseId).then(result => res.send(result));
})

// make a route to delete a course 
router.delete('/deleteCourse/:courseId', auth.verify, (req, res) => {
	courseController.deleteCourse(req.params.courseId).then(result => res.send(result));
})


module.exports = router;