const express = require('express');
const router = express.Router();

//controllers
const userController = require('./../controllers/userController');
//auth
const auth = require('./../auth');

//check if email exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExist(req.body).then(result => res.send(result))
})


//user registration
router.post('/register', (req,res) => {
	userController.register(req.body).then(result => res.send(result))
})



//login
router.post('/login', (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})

router.get('/details', auth.verify, (req, res) =>{
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData.id).then(result => res.send(result))

})

//enrollements
router.post('/enroll', auth.verify,(req, res) => {
	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId // try to use URLSearchparams
	}
	userController.enroll(data).then(result => res.send(result))
})


module.exports = router;