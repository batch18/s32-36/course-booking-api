// we will use this for authentication
	// login
	// retrieve user details

// JSON web tokens
	// methods : 
		//sign(data, secret, {})
			//creates a token
		//verify(token, secret, cb())
			//checks if token is present 
		
		//decode(token, {}).payload
			//interprets/decodes the created token

let jwt = require('jsonwebtoken');
let secret = "coursebookingAPI";


//create token
module.exports.createAccessToken = (user) => {

	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data, secret, {});
}

//verify token
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization

	if(typeof token !== "undefined"){
		token = token.slice(7);
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"})
			}
			else {
				next();
			}
		})
	}

} 

module.exports.decode = (token) => {

	if(typeof token !== "undefined"){
		token = token.slice(7);
		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return null
			}
			else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}

}



//decode(req.headers.authorization)