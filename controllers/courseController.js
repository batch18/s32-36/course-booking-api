const Course = require('./../models/Courses')


module.exports.getAllActive = () => {

	return Course.find({isActive : true}).then( result => {
		return result
	})
}

module.exports.getAll = () => {

	return Course.find().then( result => {
		return result
	})
}


module.exports.addCourse = (reqBody) => {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
	});


	return newCourse.save().then( (error, result) => {
		if(error){
			return error
		} else {
			return newCourse
		}
	});
}

// challenge
module.exports.findCourse = (params) => {
	return Course.findById(params).then( result => {
		return result
	})
}



module.exports.editCourse = (reqBody, params) => {

	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	//Model.method
	return Course.findByIdAndUpdate(params, updatedCourse, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}


module.exports.archiveCourse = (params) => {
	const update = {isActive : false};
	return Course.findByIdAndUpdate(params ,update, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}


module.exports.unarchiveCourse = (params) => {
	const update = {isActive : true};
	return Course.findByIdAndUpdate(params ,update, {new: true})
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}


module.exports.deleteCourse = (params) => {
	return Course.findByIdAndDelete(params)
	.then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

