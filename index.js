const express = require('express');
const mongoose = require('mongoose');
const port = process.env.PORT || 3000;
const app = express();
const cors = require('cors');

//middleware
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());

//mongoose connection
mongoose.connect("mongodb+srv://LaFenice:_3Valentine-4@cluster0.znk4w.mongodb.net/course_booking?retryWrites=true&w=majority", 
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
).then(()=> console.log(`connected to database`)).catch((error)=>console.log(error));

//schema



//routes
let userRoutes = require('./routes/userRoutes')
let courseRoutes = require('./routes/courseRoutes')

app.use("/api/users", userRoutes);
app.use("/api/course", courseRoutes);

app.listen(port, () =>(console.log(`server running on port ${port}`)));